<?php

/**
 * @file
 * Main file for Entity Documentation Node module.
 */

/**
 * Implements hook_ed_type().
 */
function entity_documentation_node_ed_type($types) {

  // Get node types.
  $node_types = node_type_get_types();

  $types['node'] = array(
    'name' => 'Node',
    'bundles' => array(),
  );

  foreach ($node_types as $key => $bundle) {
    $types['node']['bundles'][$key] = array(
      'name' => $bundle->name,
      'description' => $bundle->description,
      'entity' => 'node',
    );
  }

  return $types;
}

/**
 * Return documentation for specific bundle.
 *
 * @param $documentation_array
 *   Existing documentation array.
 * @param $entity
 *   Entity type.
 * @param $bundle
 *   Node bundle.
 *
 * @return array
 *   Documentation array with specific bundle in.
 */
function entity_documentation_node_ed_bundle_documentation($documentation_array, $entity, $bundle) {

  // Return documentation only for node.
  if ($entity == 'node') {
    $node_type = node_type_get_type($bundle);
    $fields = field_info_instances($entity, $bundle);

    // Menu options.
    $menu_options = implode(', ', variable_get('menu_options_' . $node_type->type, array('-')));

    // Comments.
    $comment = variable_get('comment_' . $node_type->type, '-');
    if ($comment == 0) {
      $comment = t('Hidden');
    }
    elseif ($comment == 1) {
      $comment = t('Closed');
    }
    elseif ($comment == 2) {
      $comment = t('Open');
    }

    // Language.
    $language = variable_get('language_content_type_' . $node_type->type, '-');
    if ($language == 0) {
      $language = t('Disabled');
    }
    elseif ($language == 1) {
      $language = t('Enabled');
    }
    elseif ($language == 2) {
      $language = t('Enabled, with translation');
    }

    // Node type parameters.
    $documentation_array[$entity][$bundle]['params'] = array(
      'name' => $node_type->name,
      'description' => $node_type->description,
      'properties' => array(
        'module' => array(
          'name' => t('Module'),
          'value' => $node_type->module,
        ),
        'custom' => array(
          'name' => t('Custom'),
          'value' => $node_type->custom ? t('Yes') : t('No'),
        ),
        'menu_options' => array(
          'name' => t('Menu'),
          'value' => $menu_options,
        ),
        'comment' => array(
          'name' => t('Comments'),
          'value' => $comment,
        ),
        'language' => array(
          'name' => t('Multilingual support'),
          'value' => $language,
        ),
      ),
    );

    // Field columns.
    $documentation_array[$entity][$bundle]['field_columns'] = array(
      'field' => t('Field'),
      'name' => t('Name'),
      'description' => t('Description'),
      'required' => t('Required'),
    );

    // Fields.
    // Title.
    $documentation_array[$entity][$bundle]['fields']['title'] = array(
      'field' => 'title',
      'name' => $node_type->title_label,
      'description' => '',
      'required' => t('Yes'),
    );

    foreach ($fields as $field_key => $field) {
      $documentation_array[$entity][$bundle]['fields'][$field_key] = array(
        'field' => $field_key,
        'name' => $fields[$field_key]['label'],
        'description' => $fields[$field_key]['description'],
        'required' => $fields[$field_key]['required'] ? t('Yes') : t('No'),
      );
    }
  }

  return $documentation_array;
}
