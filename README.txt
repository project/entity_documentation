README.txt
==========

A module that exports entity configuration documentation.

There is already implementation, through submodules, for node entities and
PDF, JSON export.

There are hooks to add new entities and exporters. Please check
entity_documentation.api.php

AUTHOR/MAINTAINER
======================
Author: Thanos Nokas(Matrixlord)
Maintainer: Thanos Nokas(Matrixlord) (https://drupal.org/user/1538394)
